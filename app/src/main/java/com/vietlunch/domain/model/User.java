package com.vietlunch.domain.model;

/**
 * Created by TungNQ5 on 6/14/2017.
 */

public class User {
    private String name;
    private String phoneNo;

    public User() {
    }

    public User(String name, String phoneNo) {
        this.name = name;
        this.phoneNo = phoneNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
}
