package com.vietlunch.domain.service;

import com.vietlunch.domain.model.User;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by TungNQ5 on 6/15/2017.
 */

public interface AuthenticationService {
    @POST("auth/login")
    @FormUrlEncoded
    Observable<User> login(@Field(value = "username") String username,
                           @Field(value = "password") String password);
}
