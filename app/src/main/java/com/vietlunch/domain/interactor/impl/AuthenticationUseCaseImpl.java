package com.vietlunch.domain.interactor.impl;

import com.vietlunch.domain.interactor.AuthenticationUseCase;
import com.vietlunch.domain.model.User;
import com.vietlunch.domain.repository.UserRepository;

import io.reactivex.Observable;

/**
 * Created by TungNQ5 on 6/14/2017.
 */

public class AuthenticationUseCaseImpl implements AuthenticationUseCase {

    private UserRepository userRepository;

    public AuthenticationUseCaseImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Observable<User> login(String name, String password) {
        return userRepository.login(name, password);
    }
}
