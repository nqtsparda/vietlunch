package com.vietlunch.domain.interactor;

import com.vietlunch.domain.model.User;

import io.reactivex.Observable;

/**
 * Created by TungNQ5 on 6/14/2017.
 */

public interface AuthenticationUseCase {
    Observable<User> login(String name, String password);
}
