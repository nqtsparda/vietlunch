package com.vietlunch.domain.repository;

import com.vietlunch.domain.model.User;

import io.reactivex.Observable;

/**
 * Created by TungNQ5 on 6/15/2017.
 */

public interface UserRepository {
    Observable<User> login(String name, String password);
}
