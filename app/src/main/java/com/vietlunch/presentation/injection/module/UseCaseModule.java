package com.vietlunch.presentation.injection.module;

import com.vietlunch.domain.interactor.AuthenticationUseCase;
import com.vietlunch.domain.interactor.impl.AuthenticationUseCaseImpl;
import com.vietlunch.domain.repository.UserRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by TungNQ5 on 6/14/2017.
 */

@Module
public class UseCaseModule {
    @Provides
    @Singleton
    AuthenticationUseCase provideAuthenticationUseCase(UserRepository userRepository) {
        return new AuthenticationUseCaseImpl(userRepository);
    }
}
