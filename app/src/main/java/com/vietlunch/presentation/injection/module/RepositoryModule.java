package com.vietlunch.presentation.injection.module;

import com.vietlunch.domain.repository.UserRepository;
import com.vietlunch.domain.repository.impl.UserRepositoryImpl;
import com.vietlunch.domain.service.AuthenticationService;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.inject.Singleton;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import timber.log.Timber;

/**
 * Created by TungNQ5 on 6/14/2017.
 */

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    UserRepository provideUserRepository(){
        return new UserRepositoryImpl();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        OkHttpClient client = null;

        try {
            X509TrustManager x509TrustManager = new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            };

            TrustManager[] trustCerts = new TrustManager[1];
            trustCerts[0] = x509TrustManager;
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustCerts, new java.security.SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, x509TrustManager);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            /**
             * Set this to false to avoid retrying requests when doing so is destructive.
             * In this case the calling application should do its own recovery of connectivity failures
             */
            client = builder.retryOnConnectionFailure(false).followRedirects(false).build();
        } catch (Exception e) {
            Timber.e(e, e.getMessage());
        }

        return client;
    }
}
