package com.vietlunch.presentation.injection.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.vietlunch.R;
import com.vietlunch.presentation.feature.base.appbus.AppBus;
import com.vietlunch.presentation.feature.base.appbus.AppBusImpl;
import com.vietlunch.presentation.feature.base.runtime.Runtime;
import com.vietlunch.presentation.feature.base.runtime.RuntimeImpl;
import com.vietlunch.presentation.util.security.ObscuredSharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by TungNQ5 on 6/14/2017.
 */

@Module
public class AppModule {
    private final Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    SharedPreferences provideObscuredSharedPreferences() {
        return new ObscuredSharedPreferences(mApplication,
                mApplication.getSharedPreferences(mApplication.getString(R.string.app_name),
                        Context.MODE_PRIVATE));
    }

    @Provides
    @Singleton
    AppBus provideApplicationBus() {
        return new AppBusImpl();
    }

    @Provides
    @Singleton
    Runtime provideRuntime(
            SharedPreferences sharedPref, Gson gson) {
        return new RuntimeImpl(sharedPref, gson);
    }
}
