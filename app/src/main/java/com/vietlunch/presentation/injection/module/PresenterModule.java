package com.vietlunch.presentation.injection.module;

import com.vietlunch.domain.interactor.AuthenticationUseCase;
import com.vietlunch.domain.interactor.impl.AuthenticationUseCaseImpl;
import com.vietlunch.domain.repository.UserRepository;
import com.vietlunch.presentation.feature.login.presenter.LoginPresenter;
import com.vietlunch.presentation.feature.login.presenter.LoginPresenterImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by TungNQ5 on 6/14/2017.
 */

@Module
public class PresenterModule {
    @Provides
    @Singleton
    LoginPresenter provideLoginPresenter(AuthenticationUseCase authenticationUseCase) {
        return new LoginPresenterImpl(authenticationUseCase);
    }
}
