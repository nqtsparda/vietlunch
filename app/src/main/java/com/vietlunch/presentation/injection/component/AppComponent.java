package com.vietlunch.presentation.injection.component;

import com.vietlunch.presentation.feature.base.BaseActionBarActivity;
import com.vietlunch.presentation.feature.login.view.MainActivity;
import com.vietlunch.presentation.injection.module.AppModule;
import com.vietlunch.presentation.injection.module.PresenterModule;
import com.vietlunch.presentation.injection.module.RepositoryModule;
import com.vietlunch.presentation.injection.module.UseCaseModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by TungNQ5 on 6/14/2017.
 */

@Singleton
@Component(
        modules = {
                AppModule.class,
                PresenterModule.class,
                UseCaseModule.class,
                RepositoryModule.class
        }
)
public interface AppComponent {
        void inject(BaseActionBarActivity f);

        void inject(MainActivity f);
}
