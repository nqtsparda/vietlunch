package com.vietlunch.presentation.feature.base.presenter;

/**
 * Created by TungNQ5 on 6/19/2017.
 */

public interface IBasePresenter<ViewType> {
    void setView(ViewType view);

    ViewType getView();

    void resume();

    void pause();

    void destroy();

    void updateUI();

    void destroy(long viewHashCode);
}
