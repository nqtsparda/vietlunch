package com.vietlunch.presentation.feature.login.view;

import android.os.Bundle;
import android.widget.TextView;

import com.vietlunch.R;
import com.vietlunch.presentation.feature.base.BaseActionBarActivity;
import com.vietlunch.presentation.feature.base.VietLunchApplication;
import com.vietlunch.presentation.feature.login.presenter.LoginPresenter;

import javax.inject.Inject;

import butterknife.BindView;

public class MainActivity extends BaseActionBarActivity implements LoginView {

    @BindView(R.id.txt_test)
    TextView txtTest;

    @Inject
    LoginPresenter loginPresenter;

    @Override
    public void setupActivityComponent() {
        VietLunchApplication.getAppComponent().inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loginPresenter.setView(this);
        loginPresenter.login("Tung", "Tung");
//        txtTest.setText("Hello Tung");
    }

    @Override
    public void setHelloText(String userName) {
        txtTest.setText(userName);
    }
}
