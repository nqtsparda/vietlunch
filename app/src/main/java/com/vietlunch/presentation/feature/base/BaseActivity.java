package com.vietlunch.presentation.feature.base;

/**
 * Created by TungNQ5 on 6/15/2017.
 */

public interface BaseActivity {
    void setupActivityComponent();

    int getContentResId();

    BaseFragment getContainerFragment();
}
