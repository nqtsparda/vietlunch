package com.vietlunch.presentation.feature.login.presenter;

import android.app.Activity;

import com.vietlunch.presentation.feature.login.view.LoginView;
import com.vietlunch.presentation.feature.login.view.MainActivity;

/**
 * Created by TungNQ5 on 6/16/2017.
 */

public interface LoginPresenter<T> {
    void login(String username, String password);

    void setView(T mainActivity);
}
