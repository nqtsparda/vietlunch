package com.vietlunch.presentation.feature.base.presenter;

import android.support.annotation.NonNull;

import com.vietlunch.presentation.feature.base.appbus.AppBus;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import io.reactivex.disposables.Disposable;

/**
 * Created by TungNQ5 on 6/19/2017.
 */

public class BasePresenter<T> implements IBasePresenter<T> {


    @NonNull
    protected Runtime runtime;

    @NonNull
    protected AppBus appBus;

    private volatile WeakReference<T> mView;
    private long mViewHashcode;
    protected Set<Disposable> subscriptions;

    public BasePresenter(@NonNull Runtime runtime,
                         @NonNull AppBus appBus) {
        this.runtime = runtime;
        this.appBus = appBus;
    }

    @Override
    public void setView(T viewObject) {
        final WeakReference<T> previousView = this.mView;

        if (previousView != null) {
            this.mView = null;
            //throw new IllegalStateException("Previous view is not unbounded! previousView = " + previousView);
        }

        this.mView = new WeakReference<>(viewObject);
        mViewHashcode = mView.get().hashCode();
    }

    @Override
    public T getView() {
        if (mView != null) {
            return mView.get();
        }
        return null;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        destroyAllSubscription();

        final WeakReference<T> previousView = this.mView;
        if (previousView == mView) {
            this.mView = null;
        } else {
            throw new IllegalStateException("Unexpected view! previousView = " + previousView + ", view to unbind = " + mView);
        }
    }

    @Override
    public void updateUI() {
        if (mView != null && mView.get() != null) {
//            mView.get().viewLoaded();
        }
    }

    @Override
    public void destroy(long viewHashCode) {
        if (viewHashCode == mViewHashcode) {
            destroy();
        }
    }

    protected void addOneSubscription(Disposable s) {
        if(this.subscriptions == null) {
            this.subscriptions = new HashSet();
        }

        this.subscriptions.add(s);
    }

    public void destroyAllSubscription() {
        if(this.subscriptions != null) {
            Iterator var1 = this.subscriptions.iterator();

            while(var1.hasNext()) {
                Disposable s = (Disposable)var1.next();
                if(!s.isDisposed()) {
                    s.dispose();
                }
            }
        }
    }

//    protected String getRandomHexString() {
//        Random randomno = new Random();
//        int part1 = randomno.nextInt(999);
//        if (part1 < 100) {
//            part1 = part1 + 100;
//        }
//        return DatatypeConverter.getInstance().printHexBinary(String.valueOf(part1).getBytes());
//    }
}
