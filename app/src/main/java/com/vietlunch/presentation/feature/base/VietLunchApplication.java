package com.vietlunch.presentation.feature.base;

import android.app.Application;
import android.content.Context;

import com.vietlunch.BuildConfig;
import com.vietlunch.presentation.injection.component.AppComponent;
import com.vietlunch.presentation.injection.component.DaggerAppComponent;
import com.vietlunch.presentation.injection.module.AppModule;

import timber.log.Timber;

/**
 * Created by TungNQ5 on 6/15/2017.
 */

public class VietLunchApplication extends Application {

    private AppComponent mAppComponent;

    public static VietLunchApplication getInstance() {
        return ContextSingleton.getContext();
    }

    public static AppComponent getAppComponent(){
        return VietLunchApplication.getInstance().mAppComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        ContextSingleton.setContext(this);

        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    static class ContextSingleton {
        private static VietLunchApplication mInstance;

        private ContextSingleton() {
            // hide public Context Singleton
        }

        public static void setContext(Context context) {
            mInstance = (VietLunchApplication) context;
        }

        public static VietLunchApplication getContext() {
            return mInstance;
        }
    }

}
