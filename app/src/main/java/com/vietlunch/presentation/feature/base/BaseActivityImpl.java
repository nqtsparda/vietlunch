package com.vietlunch.presentation.feature.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import com.vietlunch.R;

import butterknife.ButterKnife;

/**
 * Created by TungNQ5 on 6/15/2017.
 */

public abstract class BaseActivityImpl extends AppCompatActivity implements BaseActivity {
    protected void onCreate(Bundle savedInstanceState) {
        this.setupActivityComponent();
        super.onCreate(savedInstanceState);
        this.setContentView(this.getContentResId());
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    @Override
    public void setupActivityComponent() {

    }

    @Override
    public int getContentResId() {
        return R.layout.activity_main;
    }

    @Override
    public BaseFragment getContainerFragment() {
        return null;
    }
}
