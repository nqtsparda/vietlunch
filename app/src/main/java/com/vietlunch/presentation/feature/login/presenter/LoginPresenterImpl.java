package com.vietlunch.presentation.feature.login.presenter;

import com.vietlunch.domain.interactor.AuthenticationUseCase;
import com.vietlunch.domain.model.User;
import com.vietlunch.presentation.feature.base.appbus.AppBus;
import com.vietlunch.presentation.feature.base.runtime.Runtime;
import com.vietlunch.presentation.feature.login.view.LoginView;

import java.lang.ref.WeakReference;

import io.reactivex.Observable;

/**
 * Created by TungNQ5 on 6/16/2017.
 */

public class LoginPresenterImpl implements LoginPresenter<LoginView> {

    AuthenticationUseCase authenticationUseCase;
    private volatile WeakReference<LoginView> mView;

    public LoginPresenterImpl(Runtime runtime,
                              AppBus appBus,
                              AuthenticationUseCase authenticationUseCase) {
        this.authenticationUseCase = authenticationUseCase;
    }

    @Override
    public void login(String username, String password) {
        Observable<User> login = authenticationUseCase.login("Tung", "Tung");
        login.subscribe(user -> setHelloText(user.getName()));
    }

    @Override
    public void setView(LoginView mainActivity) {
        final WeakReference<LoginView> previousView = this.mView;

        if (previousView != null) {
            this.mView = null;
            //throw new IllegalStateException("Previous view is not unbounded! previousView = " + previousView);
        }

        this.mView = new WeakReference<>(mainActivity);
//        mViewHashcode = mView.get().hashCode();
    }

    public LoginView getView() {
        if (mView != null) {
            return mView.get();
        }
        return null;
    }

    void setHelloText(String userName){
        getView().setHelloText(userName);
    }
}
